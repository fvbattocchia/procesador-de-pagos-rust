mod routes;
mod model;
mod clients;
use routes::*;

use actix_web::{HttpServer, App};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new().service(client_balance)
        .service(new_client)
        .service(new_credit_transaction)
        .service(new_debit_transaction)
        .service(store_balances)
    })
        .bind("127.0.0.1:8080")?
        .run()
        .await
}