use actix_web::{post, get,web, HttpResponse, Responder};
use crate::model::*;
use crate::clients::*;

//receives client id and returns the balance
#[get("/client_balance/{user_id}")]
pub async fn client_balance(path: web::Path<i32>) -> impl Responder {
    let client_id = path.into_inner();
    let client_balance = get_client_balance(client_id);
    web::Json(client_balance)
}

/*
receives a client's information check that the document does not exist and if it does not exist, 
searches for the largest id that exists, generates the new id by adding one to the largest one and adds the new client
*/
#[post("/new_client")]
pub async fn new_client(data: web::Json<ClientInfo>) -> impl Responder {
    let client_info: ClientInfo = data.into_inner();
    let mut key = 0;
    if document_not_exist(client_info.document_number.to_string()){
        if let Some(key_bigger) = get_id_client_bigger() {
            key =key_bigger + 1;
        }
        add_client(key, client_info);
    } else {
        return HttpResponse::BadRequest().body("Error: Client already exists");
    }
      
    HttpResponse::Ok().body(key.to_string())
}

/*
looks for the client with the id, if it does not exist it returns error, if it exists it calculates the new balance and updates the value
todo: a better way to do this would be with references.
*/
#[post("/new_credit_transaction")]
pub async fn new_credit_transaction(data: web::Json<Transaction>) -> impl Responder {
    let transaction = data.into_inner();
    let new_balance;
    if let Some(client_balanse) =get_client_balance(transaction.client_id) {
        new_balance = client_balanse.balance + transaction.amount;
        update_balance(&transaction.client_id,new_balance);
    } else {
        return HttpResponse::BadRequest().body("Error: Client does not exists");

    }
    HttpResponse::Ok().body(new_balance.to_string())
}

/*
looks for the client with the id, if it does not exist it returns error, if it exists it calculates the new balance and updates the value
todo: a better way to do this would be with references.
*/
#[post("/new_debit_transaction")]
pub async fn new_debit_transaction(data: web::Json<Transaction>) -> impl Responder {
    let transaction = data.into_inner();
    let new_balance;
    if let Some(client_balanse) =get_client_balance(transaction.client_id) {
        new_balance = client_balanse.balance - transaction.amount;
        update_balance(&transaction.client_id,new_balance);
    } else {
        return HttpResponse::BadRequest().body("Error: Client does not exists");
    }

    HttpResponse::Ok().body(new_balance.to_string())
}

//saves in a file whose name is the date and file number the id and balance of all client, then resets the balances to 0.
#[post("/store_balances")]
pub async fn store_balances() -> impl Responder {
    let counter_files = save_balance();
    reset_balance();
    let response = format!("Balance {} was generated", counter_files);
    HttpResponse::Ok().body(response)
}