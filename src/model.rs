
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};


#[derive(Debug, Serialize, Deserialize,Clone)]
pub struct ClientBalance {
    pub client: ClientInfo,
    pub balance: f32,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ClientInfo {
    pub client_name: String,
    #[serde(serialize_with = "serialize_date", deserialize_with = "deserialize_date")]
    pub birth_date: NaiveDate,
    pub document_number: String,
    pub country: String,
}

fn serialize_date<S>(birth_date: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    serializer.serialize_str(&birth_date.format("%Y-%m-%d").to_string())
}

fn deserialize_date<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    NaiveDate::parse_from_str(&s, "%Y-%m-%d").map_err(serde::de::Error::custom)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Transaction {
    pub client_id: i32,
    pub amount: f32,
}