use crate::model::*;
use std::collections::HashMap;
use lazy_static::lazy_static;
use std::sync::Mutex;
use std::fs::File;
use std::io::{BufWriter, Write};
use chrono::{Local, Datelike};
use std::sync::atomic::{AtomicUsize, Ordering};

lazy_static! {
    //Global hashMap containing the client id as a key and the client information as a value
    pub static ref MAPA: Mutex<HashMap<i32, ClientBalance>> = Mutex::new(HashMap::new());
    //File Counter Global Variable
    pub static ref COUNTER_FILES: AtomicUsize = AtomicUsize::new(0);
}

//Add a new client to the map
pub fn add_client(client_id: i32, client_info: ClientInfo) {
    let mut map = MAPA.lock().unwrap();
    let client_balance = ClientBalance {
        balance:  0.0,
        client: client_info
    };
    map.insert(client_id, client_balance);
}

//Check if the document exists
pub fn document_not_exist(value: String) -> bool {
    let map = MAPA.lock().unwrap();
    !map.values().any(|v| v.client.document_number == value)
}

//search for the largest client id
pub fn get_id_client_bigger() -> Option<i32> {
    let map = MAPA.lock().unwrap();
    map.keys().copied().fold(None, |max, next| match max {
        Some(max) => Some(std::cmp::max(max, next)),
        None => Some(next),
    })
}

//returns the client's balance
pub fn get_client_balance(key: i32) -> Option<ClientBalance> {
    let map = MAPA.lock().unwrap();
    map.get(&key).cloned()
}

//updates the balance
pub fn update_balance(key: &i32, nuevo_valor: f32) {
    let mut map = MAPA.lock().unwrap();
    if let Some(value) = map.get_mut(key) {
        value.balance = nuevo_valor;
    }
}

//update to 0 the balance 
pub fn reset_balance() {
    let mut map = MAPA.lock().unwrap();
    for (_, value) in map.iter_mut() {
        value.balance = 0.0;
    }

}

//save the id client and the balance in a file
pub fn save_balance() -> usize {
    let map = MAPA.lock().unwrap();
    let data = Local::now();
    let counter_files = COUNTER_FILES.fetch_add(1, Ordering::Relaxed);
    let name_file = format!("{}{}{}_{}.dat", data.day(), data.month(), data.year(), counter_files);
    let file = File::create(name_file).expect("No se pudo crear el archivo");
    let mut writer = BufWriter::new(file);

    for (key, client_balance) in map.iter() {
        writeln!(&mut writer, "{} {}", key, client_balance.balance).expect("Error al escribir en el archivo");
    }

    counter_files
}
